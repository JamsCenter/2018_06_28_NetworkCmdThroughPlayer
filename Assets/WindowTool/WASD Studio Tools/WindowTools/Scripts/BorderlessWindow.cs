﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Diagnostics;
using UnityEngine;


/// <summary>
/// BorderlessWindow Class WASD Studio.
/// Add this class to remove borders of unity window
/// </summary>
public class BorderlessWindow : MonoBehaviour
{

    [DllImport("user32.dll")]
    static extern IntPtr SetWindowLong(IntPtr hwnd, int _nIndex, int dwNewLong);

    [DllImport("user32.dll")]
    static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();

    [DllImport("user32.dll")]
    static extern bool SetLayeredWindowAttributes(IntPtr hwnd, uint crKey, byte bAlpha, uint dwFlags);

    [DllImport("user32.dll")]
    static extern long GetWindowLong(IntPtr hwnd, long nIndex);

    private static long old;
    /// <summary>
    /// Initialize, save original style
    /// </summary>
    void Start()
    {
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
        old = GetWindowLong(GetForegroundWindow(), (long)GWLWindowStyles.GWL_STYLE);
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_EXSTYLE, (int)ExtendedWindowStyles.WS_EX_LAYERED);
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_STYLE, (int)old);
        SetLayeredWindowAttributes(GetForegroundWindow(), 0, 0, (int)WindowToolsAlphaFlag.LWA_COLORKEY);
        SetWindowPos(GetForegroundWindow(), 0, 0, 0, (int)Screen.width, (int)Screen.height, (int)(WindowToolsFlags.SWP_SHOWWINDOW | WindowToolsFlags.SWP_NOMOVE | WindowToolsFlags.SWP_NOSIZE));
#endif
    }


    /// <summary>
    /// Remove border of unity Window
    /// </summary>
    public static void NoBorder()
    {
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_EXSTYLE, (int)ExtendedWindowStyles.WS_EX_LAYERED);
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_STYLE, 1);
        SetLayeredWindowAttributes(GetForegroundWindow(), 0, 0, 0x1);
        SetWindowPos(GetForegroundWindow(), 0, 0, 0, (int)Screen.width, (int)Screen.height, (int)(WindowToolsFlags.SWP_SHOWWINDOW | WindowToolsFlags.SWP_NOMOVE | WindowToolsFlags.SWP_NOSIZE));
       
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_EXSTYLE, (int)ExtendedWindowStyles.WS_EX_LAYERED);
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_STYLE, 1);
        SetLayeredWindowAttributes(GetForegroundWindow(), 0, 0, 0x1);
        SetWindowPos(GetForegroundWindow(), 0, 0, 0, (int)Screen.width, (int)Screen.height, (int)(WindowToolsFlags.SWP_SHOWWINDOW | WindowToolsFlags.SWP_NOMOVE | WindowToolsFlags.SWP_NOSIZE));
#endif
    }

    /// <summary>
    /// Restore border of unity Window
    /// </summary>
    public static void ShowBorder()
    {
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_EXSTYLE, (int)ExtendedWindowStyles.WS_EX_LAYERED);
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_STYLE, (int)old);
        SetLayeredWindowAttributes(GetForegroundWindow(), 0, 0, (int)WindowToolsAlphaFlag.LWA_COLORKEY);
        SetWindowPos(GetForegroundWindow(), 0, 0, 0, (int)Screen.width, (int)Screen.height, (int)(WindowToolsFlags.SWP_SHOWWINDOW | WindowToolsFlags.SWP_NOMOVE | WindowToolsFlags.SWP_NOSIZE));

        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_EXSTYLE, (int)ExtendedWindowStyles.WS_EX_LAYERED);
        SetWindowLong(GetForegroundWindow(), (int)GWLWindowStyles.GWL_STYLE, (int)old);
        SetLayeredWindowAttributes(GetForegroundWindow(), 0, 0, (int) WindowToolsAlphaFlag.LWA_COLORKEY);
        SetWindowPos(GetForegroundWindow(), 0, 0, 0, (int)Screen.width, (int)Screen.height, (int)(WindowToolsFlags.SWP_SHOWWINDOW | WindowToolsFlags.SWP_NOMOVE | WindowToolsFlags.SWP_NOSIZE));
#endif
    }

}