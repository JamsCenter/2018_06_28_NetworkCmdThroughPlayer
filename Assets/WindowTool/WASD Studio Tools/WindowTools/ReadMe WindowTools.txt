WindowTools 
WASD Studio 
info@wasdstudio.com
4 JUN 2014

CAUTION!!! This tools only works in Windows StandAlone

For use this tools only put in your scene WindowTools prefab and Call the static methods from your script

WindowTools.cs:

	/// <summary>
    /// Places the window above all non-topmost windows. 
    /// The window maintains its topmost position even when it is deactivated
    /// </summary>
    public static void MakeTopMost()
	
Example:

	//Set window Always on top
	if (GUI.Button(new Rect(10, 30, 210, 100), "Top Most\nPlaces the window\nabove all non-topmost windows."))
	{
		WindowTools.MakeTopMost();
	}

	/// <summary>
    /// Places the window above all non-topmost windows. 
    /// The window maintains its topmost position even when it is deactivated
    /// </summary>
    /// <param name="newX">Specifies the new position of the left side of the window, in client coordinates.</param>
    /// <param name="newY">Specifies the new position of the top of the window, in client coordinates.</param>
    /// <param name="newCx">Specifies the new width of the window, in pixels.</param>
    /// <param name="newCy">Specifies the new height of the window, in pixels.</param>
    public static void MakeTopMost(int newX, int newY, int newCx, int newCy)

Example:

	//Set window Always on top width position and size
	if (GUI.Button(new Rect(10, 30, 210, 100), "Top Most\nPlaces the window\nabove all non-topmost windows."))
	{
		WindowTools.MakeTopMost(100,200,800,600);
	}

	/// <summary>
    /// Places the window at the top of the z-order.
    /// </summary>
    public static void MakeTop()
	
Example:

	//Set window on top
	if (GUI.Button(new Rect(670, 30, 210, 100), "Top\nPlaces the window\nat the top of the Z order."))
	{
		WindowTools.MakeTop();
	}

	/// <summary>
    /// Places the window at the top of the z-order.
    /// </summary>
    /// <param name="newX">Specifies the new position of the left side of the window, in client coordinates.</param>
    /// <param name="newY">Specifies the new position of the top of the window, in client coordinates.</param>
    /// <param name="newCx">Specifies the new width of the window, in pixels.</param>
    /// <param name="newCy">Specifies the new height of the window, in pixels.</param>
    public static void MakeTop(int newX, int newY, int newCx, int newCy)
	
Example:

	//Set window Always on top with postion and size
	if (GUI.Button(new Rect(670, 30, 210, 100), "Top\nPlaces the window\nat the top of the Z order."))
	{
		WindowTools.MakeTop(100,200,800,600);
	}

	/// <summary>
    /// Places the window above all non-topmost windows (that is, behind all topmost windows).
    /// </summary>
    public static void MakeNoTopMost()

Example:

	//Disable Always on top
	if (GUI.Button(new Rect(230, 30, 210, 100), "No Top Most\nPlaces the window\nabove all\nnon-topmost windows\n(behind all topmost windows)."))
	{
		WindowTools.MakeNoTopMost();
	}

	/// <summary>
    /// Places the window above all non-topmost windows (that is, behind all topmost windows).
    /// </summary>
    /// <param name="newX">Specifies the new position of the left side of the window, in client coordinates.</param>
    /// <param name="newY">Specifies the new position of the top of the window, in client coordinates.</param>
    /// <param name="newCx">Specifies the new width of the window, in pixels.</param>
    /// <param name="newCy">Specifies the new height of the window, in pixels.</param>
    public static void MakeNoTopMost(int newX, int newY, int newCx, int newCy)
	
Example:

	//Disable Always on top with position and size
	if (GUI.Button(new Rect(230, 30, 210, 100), "No Top Most\nPlaces the window\nabove all\nnon-topmost windows\n(behind all topmost windows)."))
	{
		WindowTools.MakeNoTopMost(100,200,800,600);
	}

	/// <summary>
    /// Places the window at the bottom of the z-order. 
    /// </summary>
    public static void MakeBottom()

Example:

	//Send window to bottom
	if (GUI.Button(new Rect(450, 30, 210, 100), "Bottom\nPlaces the window\nat the bottom of the Z order."))
	{
		WindowTools.MakeBottom();
	}

	/// <summary>
    /// Places the window at the bottom of the z-order. 
    /// </summary>
    /// <param name="newX">Specifies the new position of the left side of the window, in client coordinates.</param>
    /// <param name="newY">Specifies the new position of the top of the window, in client coordinates.</param>
    /// <param name="newCx">Specifies the new width of the window, in pixels.</param>
    /// <param name="newCy">Specifies the new height of the window, in pixels.</param>
    public static void MakeBottom(int newX, int newY, int newCx, int newCy)

Example:

	//Send window to bottom with position and size
	if (GUI.Button(new Rect(450, 30, 210, 100), "Bottom\nPlaces the window\nat the bottom of the Z order."))
	{
		WindowTools.MakeBottom(100,200,800,600);
	}

	/// <summary>
    /// Get the title of the Active Window
    /// </summary>
    /// <returns>The title of the Active Window</returns>
    public static string GetActiveWindowTitle()
	
Example:

	string title = WindowTools.GetActiveWindowTitle();

	/// <summary>
    /// Set new title for unity Window
    /// </summary>
    /// <param name="newTitle">The new Title</param>
    public static void SetWindowTitle(string newTitle)
	
Example:

	WindowTools.SetWindowTitle("New title");
	
	/// <summary>
    /// Hide Window (Not Minimize)
    /// </summary>
    public static void HideWindow()
	
Example:

	WindowTools.HideWindow();
	
	/// <summary>
    /// Show window
    /// </summary>
    public static void ShowWindow()
	
Example:

	WindowTools.ShowWindow();
	
	/// <summary>
    /// Make unity window transparent
    /// </summary>
    /// <param name="opacitty">Normalize opacitty level</param>
    public static void SetAlphaWindow(float opacitty)
	
Example:

	//Make window half transparent
    if (GUI.Button(new Rect(450, 150, 210, 100), "Set opacitty to 50%\n(Not work in editor)"))
    {
       WindowTools.SetAlphaWindow(0.5f);
    }
	
	/// <summary>
    /// Set new size for window
    /// </summary>
    /// <param name="cx">New width</param>
    /// <param name="cy">New height</param>
    public static void SetWindowSize(int cx, int cy)

Example:

	//Resize window
    if (GUI.Button(new Rect(10, 270, 210, 100), "Change Size to 1024 x 600"))
    {
        WindowTools.SetWindowSize(1024, 600);
    }

	/// <summary>
    /// Set new position for window
    /// </summary>
    /// <param name="x">New X coord</param>
    /// <param name="y">New Y coord</param>
    public static void SetWindowPosition(int x, int y)
	
Example:

	//Move window to 0,0
    if (GUI.Button(new Rect(450, 270, 210, 100), "Move to 0,0"))
    {
       WindowTools.SetWindowPosition(0, 0);
    }


BorderlessWindow.cs:

	/// <summary>
    /// Remove border of unity Window
    /// </summary>
    public static void NoBorder()

Example:

	//Hide Borders
    if (GUI.Button(new Rect(10, 420, 210, 100), "Hide Borders"))
    {
       BorderlessWindow.NoBorder();
    }
	
	/// <summary>
    /// Restore border of unity Window
    /// </summary>
    public static void ShowBorder()
	
Example:

	//Show Borders
    if (GUI.Button(new Rect(230, 420, 210, 100), "Show Borders"))
    {
		BorderlessWindow.ShowBorder();
    }



