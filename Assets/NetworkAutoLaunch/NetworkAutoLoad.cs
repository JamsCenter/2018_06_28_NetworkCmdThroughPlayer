﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.SceneManagement;

public class NetworkAutoLoad : MonoBehaviour
{

    public NetworkManager m_networkManager;
    public NetworkLobbyManager m_lobbyManager;


    public int m_numberPlayer;


    public void JoinOrHostBasedOnParams(string param, string value)
    {
        StartCoroutine(BlaBla(param, value));
       

    }
    public void Update()
    {
          CheckForPlayer("Default");

    }

    public IEnumerator BlaBla(string param, string value) {

        yield return new WaitForSeconds(0.1f);

        if (param.IndexOf("networkuser") >= 0)
        {
            if (m_lobbyManager == null)
                m_lobbyManager = FindObjectOfType<NetworkLobbyManager>();
            if (m_lobbyManager != null)
            {


                int v = int.Parse(value);
                if (v <= 0)
                {
                    CreateInternetMatch("Default");

                    yield return new WaitForSeconds(0.5f);


                    while (!IsMaxPlayerAttempt())
                    {
                        yield return new WaitForSeconds(0.5f);
                    }
                    StartCoroutine(ReadyToPlayCoroutine());
                }
                else
                {

                    FindInternetMatch("Default");
                    StartCoroutine(ReadyToPlayCoroutine());
                }


            }
            else
            {

                if (m_networkManager == null)
                    m_networkManager = FindObjectOfType<NetworkManager>();


                if (m_networkManager != null)
                {


                    int v = int.Parse(value);
                    if (v <= 0)
                        m_networkManager.StartHost();
                    else
                    {
                        m_networkManager.StartClient();

                    }


                }



            }



        }

        
    }

    private bool IsMaxPlayerAttempt()
    {
        return m_numberPlayer >= m_lobbyManager.maxPlayers;
    }

    private IEnumerator ReadyToPlayCoroutine()
    {
      //  while (true)
        {
            yield return new WaitForSeconds(2f);
            ReadyToPlay();
        }
    }

    private void OnInternetMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            //Debug.Log("Create match succeeded");

            MatchInfo hostInfo = matchInfo;
            NetworkServer.Listen(hostInfo, 9000);

            NetworkManager.singleton.StartHost(hostInfo);
        }
        else
        {
            Debug.LogError("Create match failed");
        }
    }

    public void CheckForPlayer(string matchName) {

        if (NetworkManager.singleton!=null && NetworkManager.singleton.matchMaker!=null)
            NetworkManager.singleton.matchMaker.ListMatches(0, 10, matchName, true, 0, 0, OnCheckForPlayerMatch);
    }

    private void OnCheckForPlayerMatch(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            //DIRTY CODE. SHOULD CHECK BASED ON THE ROOM ID IF IT IS THE RIGHT ONE
            m_numberPlayer = matches[0].currentSize;
        }
        else m_numberPlayer = 0;
        
    }

    //call this method to find a match through the matchmaker
    public void FindInternetMatch(string matchName)
    {
        NetworkManager.singleton.matchMaker.ListMatches(0, 10, matchName, true, 0, 0, OnInternetMatchList);
    }

    //this method is called when a list of matches is returned
    private void OnInternetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            if (matches.Count != 0)
            {
                //Debug.Log("A list of matches was returned");

                //join the last server (just in case there are two...)
                NetworkManager.singleton.matchMaker.JoinMatch(matches[matches.Count - 1].networkId, "", "", "", 0, 0, OnJoinInternetMatch);
            }
            else
            {
                Debug.Log("No matches in requested room!");
            }
        }
        else
        {
            Debug.LogError("Couldn't connect to match maker");
        }
    }

    //this method is called when your request to join a match is returned
    private void OnJoinInternetMatch(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if (success)
        {
            //Debug.Log("Able to join a match");

            MatchInfo hostInfo = matchInfo;
            NetworkManager.singleton.StartClient(hostInfo);
          

        }
        else
        {
            Debug.LogError("Join match failed");
        }
    }

    private static void ReadyToPlay()
    {
        Debug.Log("Hey hey");
        foreach (var player in FindObjectsOfType<NetworkLobbyPlayer>())
        {
            player.SendReadyToBeginMessage();
        }
            
            
    }

    void Start()
    {
        NetworkManager.singleton.StartMatchMaker();
    

    }

    //call this method to request a match to be created on the server
    public void CreateInternetMatch(string matchName)
    {
        NetworkManager.singleton.matchMaker.CreateMatch(matchName, 4, true, "", "", "", 0, 0, OnInternetMatchCreate);
    }
    
}