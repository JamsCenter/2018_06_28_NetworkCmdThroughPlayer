﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NetworkAutoLaunch {

public class SceneLoader : MonoBehaviour {

    public string sceneName;
    public float delay;
        
	void Start () {
        Invoke("Load", delay);
	}
	

    public void Load()
    {
        SceneManager.LoadScene(sceneName);
    }
}

}
