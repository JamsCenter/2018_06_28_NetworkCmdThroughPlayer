﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParamsToWindowPosition : MonoBehaviour {

    public int m_windowIndex = 1;
    public int m_lengthCount = 2;
    public int m_heightCount=3;
    public int screenWidth=1600;
    public int screenHeight=800;

    public Text m_textDebug;

    public float m_windowToolDelay = 0.3f;    

    public void DefineWindowPosition(string param, string value) {
        if (param.IndexOf("networkuser")>=0)
        {


            WindowTools.SetWindowTitle("Arg Found");
            if (m_textDebug != null)
                m_textDebug.text += "" + "ARG "  + ": " + param + " " + value + "\n\r";

            int userId = int.Parse(value);

                m_windowIndex = userId;
            Invoke("UpdateWindowPositionBasedOnIndex", m_windowToolDelay);
        }
    }





    public void UpdateWindowPositionBasedOnIndex()
    {
        Debug.Log("Tick");
        float height = (float)screenWidth / (float)m_heightCount;
        float width = (float)screenHeight / (float)m_lengthCount;
        int x;
        int y;
        GetXY(out x, out y, m_lengthCount, m_windowIndex);

        string title = string.Format("{0} {1} ({2}x{3})", (int)(x * height), (int)(width), width, height);
        Debug.Log(string.Format("{0} {1} ({2}x{3})", x, y, width, height));
        Debug.Log(title);

        SetWindowPosition(height, width, x, y, title);


        // m_windowIndex++;
        // m_windowIndex %= m_heightCount * m_lengthCount;
    }

    private static void SetWindowPosition(float height, float width, int x, int y, string title)
    {
        WindowTools.MakeTop();
        WindowTools.SetWindowPosition((int)(x * width), (int)(y * height));
        WindowTools.SetWindowSize((int)(width), (int)(height));
        WindowTools.SetWindowTitle(title);
        WindowTools.MakeTop();
    }
    public bool m_useScreenSize;
    // Update is called once per frame
    public void GetXY(out int x, out int y, int width, int index) {
        x = index / width;
        y = index % width ;
	}
    public void OnValidate()
    {
        if (m_useScreenSize)
        {
            screenWidth = Screen.currentResolution.width;
            screenHeight = Screen.currentResolution.height;

        }
    }
    public void Start() {
        if (m_useScreenSize)
        {
            screenWidth = Screen.currentResolution.width;
            screenHeight = Screen.currentResolution.height;

        }
    }
}
