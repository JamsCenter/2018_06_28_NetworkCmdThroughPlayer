﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DownloadPackage : MonoBehaviour
{

    public static string m_packageFormat = "https://gitlab.com/JamsCenter/{0}/raw/master/Package/{1}.unitypackage";

    [MenuItem("Window/Jams.Center/Download/Core")]
    public static void Download_Core()
    {
        Application.OpenURL("https://gitlab.com/JamsCenter/ToolBox/raw/release/Package/CoreToolBox.unitypackage");
    }


    [MenuItem("Window/Jams.Center/Download/Modules/Transform Replay")]
    public static void Download_Module_TransformReplay()
    {
        DownloadPackageFrom("2018_06_20_Module_TransformReplay", "TransformReplay");
    }

    [MenuItem("Window/Jams.Center/Download/Modules/Bip On Tune")]
    public static void Download_Module_BipOnTune()
    {
        DownloadPackageFrom("2018_05_16_Module_BipOnTune", "BipOnTune");

    }
    [MenuItem("Window/Jams.Center/Download/Modules/Unity Random")]
    public static void Download_Module_UnityRandom()
    {
        DownloadPackageFrom("2018_06_20_Module_UnityRandom", "UnityRandom");
    }
    [MenuItem("Window/Jams.Center/Download/Modules/Debug Draw")]
    public static void Download_Module_DebugDraw()
    {
        DownloadPackageFrom("2018_06_21_Module_DebugDraw", "DebugDraw");
    }
    //[MenuItem("Window/Jams.Center/Download/Modules/Video Trigger")]
    //public static void Download_Module_VideoTrigger()
    //{
    //    DownloadPackageFrom("2018_06_02_Module_VideoTrigger", "VideoTrigger");

    //}



    private static void DownloadPackageFrom(string repoName, string packageName)
    {

        Application.OpenURL(string.Format(m_packageFormat, repoName, packageName));
    }

}
