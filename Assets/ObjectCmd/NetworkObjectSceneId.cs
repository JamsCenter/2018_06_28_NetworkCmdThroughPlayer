﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkObjectSceneId : MonoBehaviour {

    public static Dictionary<string, NetworkObjectSceneId> m_registered = new Dictionary<string, NetworkObjectSceneId>();

    public static NetworkObjectSceneId Find(string id) {
        if (!m_registered.ContainsKey(id))
            return null;
        return m_registered[id];
    }


    [SerializeField]
    private string m_id;
    public void Reset()
    {
        m_id ="Id "+ UnityRandom.Random(1000000);
    }
    public string GetId() {
        return m_id;
    }



    public void Awake()
    {
        if (m_registered.ContainsKey(GetId()))
            Debug.LogError("They is two objects in the scene with the same ID !!!", this.gameObject);
        m_registered.Add(GetId(), this);
    }
    public void OnDestroy()
    {
        m_registered.Remove(GetId());
    }
}
