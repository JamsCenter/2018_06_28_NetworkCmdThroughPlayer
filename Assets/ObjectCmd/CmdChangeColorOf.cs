﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CmdChangeColorOf : NetworkBehaviour {

    public static CmdChangeColorOf singleton;
    [SerializeField]

    [Header("Debug")]
    private  CmdChangeColorOf singletonDebug;
    public bool m_isLocal;
    public bool m_hasAuthority;


    public void Start()
    {
        if (isLocalPlayer) {
            singleton = this;
            singletonDebug = this;
        }
        m_isLocal = isLocalPlayer;
        m_hasAuthority = hasAuthority;
        //else Destroy(this);
    }

    public  void ChangeColor(string id, Color color)
    {
        CmdChangeColor(id, color.r, color.g, color.b);

        Debug.Log(string.Format("Called ({0}): {1} {2} {3}", id, color.r, color.g, color.b));
    }

    [Command]
    public void CmdChangeColor(string id, float r, float g, float b)
    {

        Debug.Log(string.Format("CMD ({0}): {1} {2} {3}", id, r, g, b));
        RpcChangeColor(id ,r,g,b);
    }

    [ClientRpc]
    private void RpcChangeColor(string id, float r, float g, float b)
    {
        Debug.Log(string.Format("RPC ({0}): {1} {2} {3}", id, r, g, b));
        NetworkObjectSceneId obj =  NetworkObjectSceneId.Find(id);
        if (obj != null) {
            ChangeColor colorChanger = obj.GetComponent<ChangeColor>();
            if (colorChanger != null)
            {
                colorChanger.ChangeWithColor(new Color(r, g, b));

            }

        }
    }
    
}
