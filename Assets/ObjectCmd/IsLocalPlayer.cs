using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class IsLocalPlayer : NetworkBehaviour
{

    [SerializeField]
    private bool _isLocalPlayer;
    [SerializeField]
    private bool _isServer;
    [SerializeField]
    private bool _hasAutority;
    private NetworkIdentity _networkIdentity;

    public UnityEvent _ifIsLocalPlayer;
    public UnityEvent _ifIsOtherPlayer;
    public UnityEvent _ifIsServer;
    public UnityEvent _ifIsServerAndHasAutority;

    public void Start()
    {
        _networkIdentity = GetComponent<NetworkIdentity>();
        _isLocalPlayer = _networkIdentity.isLocalPlayer;
        _isServer = _networkIdentity.isServer;
        _hasAutority = _networkIdentity.hasAuthority;

        if (_isLocalPlayer)
            _ifIsLocalPlayer.Invoke();
        else _ifIsOtherPlayer.Invoke();

        if (_isServer)
            if (_hasAutority)
                _ifIsServerAndHasAutority.Invoke();
            else _ifIsServer.Invoke();
    }
}